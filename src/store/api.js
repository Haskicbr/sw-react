import axios from 'axios';

class Api {

    constructor() {
        this.API_FILMS_URL = 'https://swapi.dev/api/films/';
    }

    /**
     * @returns {string}
     */
    getUrlFilms() {
        return this.API_FILMS_URL;
    }


    /**
     * @returns {Promise<void>} 
     */
    getFilms() {
        return axios.get(this.getUrlFilms());
    }
}

const api = new Api();


export {
    api
};