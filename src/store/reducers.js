import { combineReducers } from 'redux'
import { actionTypes } from './actions'

const initialState = {

    reviews: [
        {
            filmTitle: 'The Empire Strikes Back',
            username: 'some name',
            email: 'some@gmail.com',
            review: 'this is fine move'
        }
    ],

    filmList: [

    ],

    filmView: {

    },

    isVisibleReviewForm: false,
};

function filmReducers(state = initialState, action) {
    switch (action.type) {
        case actionTypes.ACTION_SET_FILM_VIEW:
            state.filmView = action.film;
            return {
                ...state
            };
        case actionTypes.ACTION_SET_FILM_LIST:
            state.filmList = action.filmList;
            return {
                ...state
            };

        case actionTypes.ACTION_SET_VISIBLE_REVIEW_FORM:
            state.isVisibleReviewForm = action.isVisible;
            return {
                ...state
            };

        case actionTypes.ACTION_ADD_REVIEW:
            state.reviews = [action.review, ...state.reviews]
            return {
                ...state
            };

        default:
            return state;
    }
}

const rootReducer = combineReducers({
    film: filmReducers
});

export default rootReducer