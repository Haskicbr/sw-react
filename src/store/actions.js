import { api } from './api';

export const actionTypes = {
    ACTION_SET_FILM_LIST: 'ACTION_SET_FILM_LIST',
    ACTION_SET_FILM_VIEW: 'ACTION_SET_FILM_VIEW',
    ACTION_SET_VISIBLE_REVIEW_FORM: 'ACTION_SET_VISIBLE_REVIEW_FORM',
    ACTION_ADD_REVIEW: 'ACTION_ADD_REVIEW',
}

export const setFilmListAction = (filmList) => {
    return { type: actionTypes.ACTION_SET_FILM_LIST, filmList };
}

export const setFilmViewAction = (film) => {
    return { type: actionTypes.ACTION_SET_FILM_VIEW, film }
}

export const setVisibleReviewForm = (isVisible) => {
    return { type: actionTypes.ACTION_SET_VISIBLE_REVIEW_FORM, isVisible }
}

export const addReview = (review) => {
    return { type: actionTypes.ACTION_ADD_REVIEW, review }
}

export const addReviewAsync = (review) => {
    return (dispatch) => {
        new Promise((res) => {
            setTimeout(() => {
                res({ status: 200 })
            }, 1000);
        }).then(res => {
            dispatch(addReview(review));
        })
    }
}


export const getFilmsAsync = () => {
    return (dispatch) => {
        api.getFilms().then(res => {
            const filmList = res.data.results;
            dispatch(setFilmListAction(filmList));
        });
    };
}