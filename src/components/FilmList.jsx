import React from 'react';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import { connect } from 'react-redux';
import { getFilmsAsync, setFilmViewAction, setVisibleReviewForm } from '../store/actions';

class FilmList extends React.Component {

    constructor(props) {
        super(props);
        this.props.getFilmsAsync();
    }

    handleView = (film) => {
        this.props.setVisibleReviewForm(false);
        this.props.setFilmViewAction(film);
    }

    render() {
        return (
            <>
                <List>
                    {this.props.filmList.map((film, key) => {
                        return (
                            <ListItem key={key} onClick={() => { this.handleView(film) }} role={undefined} dense button>
                                <ListItemText id={film.id} primary={film.title} />
                            </ListItem>
                        );
                    })}
                </List>
            </>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        filmList: state.film.filmList
    }
}

const mapDispatchToProps = {
    getFilmsAsync,
    setFilmViewAction,
    setVisibleReviewForm
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(FilmList);
