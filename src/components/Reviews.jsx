import React from 'react';
import { connect } from 'react-redux';
import ReviewItem from './ReviewItem';
import List from '@material-ui/core/List';

class Reviews extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {

        let items = [];

        this.props.reviews
            .filter((review) => review.filmTitle === this.props.filmView.title)
            .forEach((review, key) => {
                items.push(
                    <ReviewItem
                        key={key}
                        review={review}
                    />
                )
            });

        return (
            <>
                <List>
                    {items}
                </List>
            </>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        filmView: state.film.filmView,
        reviews: state.film.reviews,
    }
}

export default connect(
    mapStateToProps,
    {}
)(Reviews);
