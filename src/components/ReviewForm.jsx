import React from 'react';
import { connect } from 'react-redux';
import Icon from '@material-ui/core/Icon';
import Button from '@material-ui/core/Button';
import { setVisibleReviewForm, addReviewAsync } from '../store/actions';
import TextField from '@material-ui/core/TextField';

class ReviewForm extends React.Component {

    constructor(props) {
        super(props);
        this.emailInput = React.createRef();
        this.reviewInput = React.createRef();
        this.usernameInput = React.createRef();

        this.state = {
            validation: {
                username: {
                    errors: []
                },
                email: {
                    errors: []
                },
                review: {
                    errors: []
                }
            },
            isInvalidForm: true
        }
    }

    validateForm = () => {

        const validation = this.state.validation;

        let isInvalidForm = false;

        const fieldValues = {
            username: this.usernameInput.current.value,
            email: this.emailInput.current.value,
            review: this.reviewInput.current.value
        }

        for (let fieldName in fieldValues) {

            fieldValues[fieldName] = fieldValues[fieldName].trim();

            let fieldValue = fieldValues[fieldName];

            validation[fieldName].errors = [];

            if (fieldValue.length === 0) {
                validation[fieldName].errors.push('The field must be required');
                isInvalidForm = true;
            }

            if (fieldName === 'email') {
                if (fieldValue.match(/\S+@\S+\.\S+/s) === null) {
                    validation[fieldName].errors.push('The field must contain a valid email address');
                    isInvalidForm = true;
                }
            }
        }

        return {
            validation,
            isInvalidForm,
            fieldValues
        }
    }


    handleSendForm = (event) => {
        event.preventDefault();
        let { validation, isInvalidForm, fieldValues } = this.validateForm();

        if (isInvalidForm) {

            this.setState({
                validation,
                isInvalidForm
            });

        } else {

            this.props.addReviewAsync(
                {
                    filmTitle: this.props.filmView.title,
                    ...fieldValues
                }
            );

            this.props.setVisibleReviewForm(false);
        }
    }

    render() {
        const filedStyle = { width: '100%', marginBottom: '20px' };
        return (
            <form onSubmit={this.handleSendForm} >
                <TextField
                    style={filedStyle}
                    id="review-email"
                    label="Email"
                    defaultValue=""
                    variant="outlined"
                    helperText={this.state.validation.email.errors.join(' ')}
                    error={this.state.validation.email.errors.length > 0}
                    inputRef={this.emailInput}
                />

                <TextField
                    style={filedStyle}
                    id="review-username"
                    label="Username"
                    defaultValue=""
                    variant="outlined"
                    helperText={this.state.validation.username.errors.join(' ')}
                    error={this.state.validation.username.errors.length > 0}
                    inputRef={this.usernameInput}
                />

                <TextField
                    style={filedStyle}
                    id="review-text"
                    label="Review text"
                    multiline
                    rows={2}
                    defaultValue=""
                    variant="outlined"
                    helperText={this.state.validation.review.errors.join(' ')}
                    error={this.state.validation.review.errors.length > 0}
                    inputRef={this.reviewInput}
                />

                <Button
                    type="submit"
                    variant="contained"
                    color="primary"
                    endIcon={<Icon>send</Icon>}
                >Send</Button>
            </form>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        filmView: state.film.filmView,
        isVisibleReviewForm: state.film.isVisibleReviewForm
    }
}

const mapDispatchToProps = {
    setVisibleReviewForm,
    addReviewAsync
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ReviewForm);
