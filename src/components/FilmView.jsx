import React from 'react';
import { connect } from 'react-redux';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Icon from '@material-ui/core/Icon';
import Button from '@material-ui/core/Button';
import { setVisibleReviewForm } from '../store/actions';
import ReviewForm from './ReviewForm';
import Reviews from './Reviews';

class FilmView extends React.Component {

    constructor(props) {
        super(props);
    }

    handleOpenForm = () => {
        this.props.setVisibleReviewForm(true);
    }

    render() {

        return (
            <>
                <List>
                    <ListItem>
                        <ListItemText
                            primary={this.props.filmView.title}
                        />
                    </ListItem>

                    <ListItem>
                        <ListItemText
                            primary={this.props.filmView.opening_crawl}
                        />
                    </ListItem>

                    {!this.props.isVisibleReviewForm && this.props.filmView.title &&
                        <ListItem>
                            <Button
                                onClick={this.handleOpenForm}
                                variant="contained"
                                color="primary"
                                endIcon={<Icon>send</Icon>}
                            >add new review</Button>
                        </ListItem>
                    }

                    {this.props.isVisibleReviewForm && <ReviewForm />}

                </List>
                {JSON.stringify(this.props.reviews)}
                {this.props.filmView.title && <Reviews />}
            </>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        filmView: state.film.filmView,
        isVisibleReviewForm: state.film.isVisibleReviewForm,
        reviews: state.film.reviews,
    }
}

const mapDispatchToProps = {
    setVisibleReviewForm
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(FilmView);
