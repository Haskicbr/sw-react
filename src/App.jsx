import React from 'react';
import Grid from '@material-ui/core/Grid';
import CssBaseline from '@material-ui/core/CssBaseline';
import Container from '@material-ui/core/Container';
import reducers from './store/reducers';
import { createStore, compose, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';


import FilmList from './components/FilmList';
import FilmView from './components/FilmView';

const store = createStore(reducers, compose(applyMiddleware(thunk), window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()));

export default class App extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        return <Provider store={store}>
            <React.Fragment>
                <CssBaseline />
                <Container maxWidth="md">
                    <Grid container spacing={3}>
                        <Grid item xs={6}>
                            <FilmList />
                        </Grid>
                        <Grid item xs={6}>
                            <FilmView />
                        </Grid>
                    </Grid>
                </Container>
            </React.Fragment>
        </Provider>;
    }
}